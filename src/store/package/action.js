import _ from "lodash";
import * as types from "./actionTypes";
import {
  loadPackages
} from "../../requests/packages";
import queryFilters from "utils/query-filters";

export function fetchpackages(params) {
  return async (dispatch, getState) => {
    dispatch(types.fetchUsersBegin());
    return await loadPackages(params)
      .then(json => {
        dispatch(types.fetchpackagesSuccess(json));
      })
      .catch(error => dispatch(types.fetchPackagesFailure(error)));
  };
}