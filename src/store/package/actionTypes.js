export const RESET_ALL = "RESET_ALL";

export const resetAll = () => ({
  type: RESET_ALL
});

// ---------------------------------Fetch Packages -------------------------------------------------

export const FETCH_PACKAGES_BEGIN = "FETCH_PACKAGES_BEGIN";
export const FETCH_PACKAGES_SUCCESS = "FETCH_PACKAGES_SUCCESS";
export const FETCH_PACKAGES_FAILURE = "FETCH_PACKAGES_FAILURE";

export const fetchPackagesBegin = () => ({
  type: FETCH_PACKAGES_BEGIN
});

export const fetchPackagesSuccess = users => ({
  type: FETCH_PACKAGES_SUCCESS,
  payload: { users }
});

export const fetchPackagesFailure = error => ({
  type: FETCH_PACKAGES_FAILURE,
  payload: { error }
});