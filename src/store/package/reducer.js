import _ from "lodash";
import * as types from "./actionTypes";

const initialState = {
    usersArray: [],
    userDetail: {},
    userData: {},
    isLoading: false,
    error: null,
    isUserUpdated: false,
    isUserCreated: false,
    isValidationError: false,
    meta: {},
  };
  
  export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
      case types.RESET_ALL:
        return initialState;
        break;
  
      //Fetch Users
  
      case types.FETCH_PACKAGES_BEGIN:
        return {
          ...state,
          isLoading: true,
          error: null,
        };
        break;
  
      case types.FETCH_PACKAGES_SUCCESS:
        return {
          ...state,
          isLoading: false,
          userData: {},
          usersArray: action.payload.users.data,
          meta: action.payload.users.meta,
          isUserUpdated: false,
          error: {},
          isUserCreated: false,
          isValidationError: false,
        };
        break;
  
      case types.FETCH_PACKAGES_FAILURE:
        return {
          ...state,
          isLoading: false,
          error: action.payload.error,
          usersArray: [],
          meta: {},
        };
        break;
    }
}