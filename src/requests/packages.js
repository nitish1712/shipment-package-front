import axios from "axios";

export function loadPackages(params = {}) {
  return axios
    .get(process.env.REACT_APP_API_URL + "/api/users", { params })
    .then(response => response.data);
}